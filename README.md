Ian Wyatt and Mike Isasi
i2891405
System Software Summer 2014
Homework 4

Note:
	This program reads PL/0 of the following EDNF rules:

	program :: = block "." .
	block :: = const - declaration  var - declaration proc - declaration statement.
	constdeclaration :: = [“const” ident "=" number{ "," ident "=" number } “; "].
	var-declaration  :: = ["var" ident{ "," ident } “; "].							// int
	proc-declaration:: = { "procedure" ident ";" block ";" } statement .
	statement   :: = [
	ident ":=" expression
	| "call" ident
	| "begin" statement{ ";" statement } "end"
	| "if" condition "then" statement[“else" statement]
	| "while" condition "do" statement
	| “read” ident
	| “write” ident
	| e															// empty
	] .
	condition ::= "odd" expression
	| expression  rel-op  expression.

	rel-op ::= "="|“<>"|"<"|"<="|">"|">=“.
	expression ::= [ "+"|"-"] term { ("+"|"-") term}.
	term ::= factor {("*"|"/") factor}.
	factor ::= ident | number | "(" expression ")“.
	number ::= digit {digit}.
	ident ::= letter {letter | digit}.
	digit ;;= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9“.
	letter ::= "a" | "b" | … | "y" | "z" | "A" | "B" | ... | "Y" | "Z".

	Reserved Words: const, var, procedure, call, begin, end, if, then, else, while, do, read, write.
	Special Symbols: ‘+’, ‘-‘, ‘*’, ‘/’, ‘(‘, ‘)’, ‘=’, ’,’ , ‘.’, ‘ <’, ‘>’,  ‘;’ , ’:’ .
	Identifiers: identsym = letter (letter | digit)*
	Numbers: numbersym = (digit)+
	Invisible Characters: tab, white spaces, newline
	Comments denoted by: /* . . .   /

Usage:
	In the same folder as compiler.c you must have an input file, named "input.txt".
	The input must be a PL/0 program which follow the rules of the PL/0 EDNF.

	In order to compile in a UNIX based environment:
	  In Terminal:
		To compile: gcc -Wall scanner.c pmachine.c compiler.c –o hw3isaasiwyatt
		To Run: ./hw3isaasiwyatt
		Optional Arguments:
			-l	: print the list of lexemes/tokens (scanner output) to the screen
			-a	: print the generated assembly code (parser/codegen output) to the screen
			-v	: print virtual machine execution trace (virtual machine output) to the screen
			

	Program Output and errors will be printed to the console. 
	Depending on the PL/0 program input, the user may be prompted to enter a number by the PL/0 VM. Only after entering a number will the program continue.
	After the output is printed, the user will be prompted to enter any input, at which point the program will stop executing.