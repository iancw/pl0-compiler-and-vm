/*
Ian Wyatt

This program uses lexical analization to break an inpuit high level code in to lexigraphical tokens
The tokens will be sent to a syntax analyzer (parse - HW3)
*/

// allows compilation in windows
#ifdef _WIN32 
#define _CRT_SECURE_NO_DEPRECATE 
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "scanner.h"

Symbol symbol_table[MAX_SYMBOL_TABLE_SIZE];
Token declarationToken;
static int lexLevel = 0;

// convert TokenEnum to string
const char *getTokenString(Token t)
{
	const char *strings[] = { "NON",
		"nulsym", "identsym", "numbersym", "plussym", "minussym", "multsym", "slashsym",
		"oddsym", "eqlsym", "neqsym", "lessym", "leqsym", "gtrsym", "geqsym",
		"lparentsym", "rparentsym", "commasym", "semicolonsym", "periodsym", "becomessym",
		"beginsym", "endsym", "ifsym", "thensym", "whilesym", "dosym", "callsym",
		"constsym", "varsym", "procsym", "writesym", "readsym", "elsesym"
	};

	return strings[t];
}

int isAlpha(char c) {
	return((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
}

int isNumeric(char c) {
	return(c >= '0' && c <= '9');
}

int isWhitespace(char c) {
	return(c == ' ' || c == '\n' || c == '\t' || c == '\0');
}

int isSymbol(char c) {
	return(c == '=' || c == '<' || c == '>' || c == '/' || c == '+' || c == '-' || c == '*' || c == '(' || c == ')' || c == ',' || c == '.' || c == ';' || c == ':' || c == '.');
}

int getToken(char tokenStr[MAX_IDENTIFIER_LENGTH], Token lastToken, int* symbolIndex) {
	if (strcmp("null", tokenStr) == 0) {
		return nulsym;
	}
	else if (strcmp("+", tokenStr) == 0) {
		return plussym;
	}
	else if (strcmp("-", tokenStr) == 0) {
		return minussym;
	}
	else if (strcmp("*", tokenStr) == 0) {
		return multsym;
	}
	else if (strcmp("/", tokenStr) == 0) {
		return slashsym;
	}
	else if (strcmp("odd", tokenStr) == 0) {
		return oddsym;
	}
	else if (strcmp("=", tokenStr) == 0) {
		return eqlsym;
	}
	else if (strcmp("<>", tokenStr) == 0) {
		return neqsym;
	}
	else if (strcmp("<", tokenStr) == 0) {
		return lessym;
	}
	else if (strcmp("<=", tokenStr) == 0) {
		return leqsym;
	}
	else if (strcmp(">", tokenStr) == 0) {
		return gtrsym;
	}
	else if (strcmp(">=", tokenStr) == 0) {
		return geqsym;
	}
	else if (strcmp("(", tokenStr) == 0) {
		return lparentsym;
	}
	else if (strcmp(")", tokenStr) == 0) {
		return rparentsym;
	}
	else if (strcmp(",", tokenStr) == 0) {
		return commasym;
	}
	else if (strcmp(";", tokenStr) == 0) {
		declarationToken = 0;
		return semicolonsym;
	}
	else if (strcmp(".", tokenStr) == 0) {
		return periodsym;
	}
	else if (strcmp(":=", tokenStr) == 0) {
		return becomessym;
	}
	else if (strcmp("begin", tokenStr) == 0) {
		return beginsym;
	}
	else if (strcmp("end", tokenStr) == 0) {
		return endsym;
	}
	else if (strcmp("if", tokenStr) == 0) {
		return ifsym;
	}
	else if (strcmp("then", tokenStr) == 0) {
		return thensym;
	}
	else if (strcmp("while", tokenStr) == 0) {
		return whilesym;
	}
	else if (strcmp("do", tokenStr) == 0) {
		return dosym;
	}
	else if (strcmp("call", tokenStr) == 0) {
		return callsym;
	}
	else if (strcmp("const", tokenStr) == 0) {
		declarationToken = constsym;
		return constsym;
	}
	else if (strcmp("var", tokenStr) == 0) {
		declarationToken = varsym;
		return varsym;
	}
	else if (strcmp("procedure", tokenStr) == 0) {
		declarationToken = procsym;
		return procsym;
	}
	else if (strcmp("write", tokenStr) == 0) {
		return writesym;
	}
	else if (strcmp("read", tokenStr) == 0) {
		return readsym;
	}
	else if (strcmp("else", tokenStr) == 0) {
		return elsesym;
	}

	else {
		// ident sym or num sym
		unsigned int x;
		int n;
		int thisToken = 0;
		for (x = 0; x < strlen(tokenStr); x++) {
			if (isNumeric(tokenStr[x])) {
				if (x == 0) thisToken = numbersym;
			}
			else if (isAlpha(tokenStr[x])) {
				if (x != 0 && thisToken == numbersym) {
					// an identity may not start with a number
					thisToken = -2;
					break; // escape while loop
				}
			}
			else {
				// invalid symbol. How did we get this far?
				thisToken = -1;
				break; // escape while loop
			}
		}
		// add to symbol table
		// init symbol
		symbol_table[*symbolIndex].Address = *symbolIndex;
		symbol_table[*symbolIndex].Level = lexLevel;
		symbol_table[*symbolIndex].Value = 0;
		if (thisToken == numbersym) {
			// add number to symbol table
			symbol_table[*symbolIndex].Type = Number;
			symbol_table[*symbolIndex].Value = atoi(tokenStr);
			for (n = 0; n < MAX_IDENTIFIER_LENGTH; n++) {
				symbol_table[*symbolIndex].Name[n] = tokenStr[n];
			}
			(*symbolIndex)++;
		}
		else if (declarationToken == 0) {
			thisToken = identsym;
			// we are not declaring vars, dont add to symbol table
		}
		else {
			thisToken = identsym;

			for (n = 0; n < MAX_IDENTIFIER_LENGTH; n++) {
				symbol_table[*symbolIndex].Name[n] = tokenStr[n];
			}

			if (declarationToken == procsym) {
				symbol_table[*symbolIndex].Type = Procedure;
				lexLevel++;
			}
			else if (declarationToken == constsym) {
				symbol_table[*symbolIndex].Type = Constant;
			}
			else if (declarationToken == varsym) {
				symbol_table[*symbolIndex].Type = Variable;
			}
			(*symbolIndex)++;
		}
		return thisToken;
	}

	return -1; // invalid symbol
}

// search through the symbol table for the symbol with the specified name
int findSymbolIndex(char* symbolName, int symbolIndex) {
	int x = 0;
	int lexLevelDroppedto = lexLevel;
	for (x = symbolIndex - 1; x >= 0; x--) {
		// dont search back up levels, they are not in scope
		if (symbol_table[x].Level <= lexLevelDroppedto) {
			if (strcmp(symbolName, symbol_table[x].Name) == 0)
				return x;
			else if (symbol_table[x].Level < lexLevelDroppedto)
				lexLevelDroppedto--;
		}
	}
	return -1; // error
}

// search through the symbol table for the symbol with the specified value
int findNumberIndex(int symbolValue) {
	int x = 0;
	for (x = 0; x < MAX_SYMBOL_TABLE_SIZE; x++) {
		if (symbol_table[x].Value == symbolValue)
			return x;
	}
	return -1; // error
}

void addCharToToken(int *tokenIndex, char* tokenStr, char charToAdd) {
	tokenStr[*tokenIndex] = charToAdd;
	(*tokenIndex)++;
}

int scan(char* inFileName, int enableOutput, int lexemeList[MAX_NUMBER_LEXEMES*MAX_NUMBER_LENGTH], Symbol symbolTable[MAX_SYMBOL_TABLE_SIZE])
{
	int symbolIndex = 0;
	FILE* inFile = fopen(inFileName, "r");
	int commentMode = 0;
	Token lastToken = -1;
	Token beforeLastToken = -1;
	int tokenEnd = 0;
	char lastChar = -1;
	char tokenStr[MAX_IDENTIFIER_LENGTH];
	char nextChar = -1;
	int tokenIndex = 0;
	int lexemes[MAX_NUMBER_LEXEMES + MAX_SYMBOL_TABLE_SIZE];
	int lexemeIndex = 0;
	int foundSymbol = 0;

	//fprintf(stdout, "Lexeme Table:\nlexeme\ttoken type\n");

	nextChar = fgetc(inFile);

	while (!feof(inFile)) {
		// if it is whitespace and we have a token, end it
		if (isWhitespace(nextChar)) {
			if (tokenIndex > 0) {
				// end token
				tokenEnd = 1;
			}
		}
		else if (commentMode) {
			// look ahead for comment end	
			nextChar = fgetc(inFile);
			while (!feof(inFile) && commentMode) {
				if (nextChar == '*') {
					nextChar = fgetc(inFile);
					if (!feof(inFile)) {
						if (nextChar == '/')
							commentMode = 0;
					}
				}
				nextChar = fgetc(inFile);
			}
		}
		else if (isAlpha(nextChar) || isNumeric(nextChar)) {
			// can be : constdeclaration, var-declaration, proc-declaration, statement, ident
			if (isSymbol(lastChar)) tokenEnd = 1; // a symbol indicates we are done with this one 
			else addCharToToken(&tokenIndex, tokenStr, nextChar);
		}
		else if (isSymbol(nextChar)) {
			if (!isSymbol(lastChar)) tokenEnd = 1; // a non symbol indicates we are done with this one
			else addCharToToken(&tokenIndex, tokenStr, nextChar);
		}
		else {
			// FAIL, print error
			if (enableOutput) {
				fprintf(stdout, "Error, %c is an invalid PL/0 symbol.", nextChar);
				return EXIT_FAILURE;
			}
			break; // escape while loop
		}

		// if a token is ended or a token is empty
		if (tokenEnd) {
			// if our token is ended but we dont have a token, create it
			if (tokenIndex == 0) {
				addCharToToken(&tokenIndex, tokenStr, nextChar);
			}
			else {
				// complete token, add it to token list
				tokenStr[tokenIndex] = '\0'; // end string

				// detect comment
				if (strcmp(tokenStr, "/*") == 0) {
					tokenIndex = 0;
					commentMode = 1;
				}
				else {
					int thisToken = getToken(tokenStr, lastToken, &symbolIndex);
					if (thisToken == identsym && strlen(tokenStr) > MAX_IDENTIFIER_LENGTH) {
						if (enableOutput) {
							fprintf(stdout, "Error, %s is over the %d identifier length limit\n", tokenStr, MAX_IDENTIFIER_LENGTH);
							return EXIT_FAILURE;
						}
						break; // escape while loop
					}
					else if (thisToken == numbersym && strlen(tokenStr) > MAX_NUMBER_LENGTH) {
						if (enableOutput) {
							fprintf(stdout, "Error, %s is over the %d number length limit\n", tokenStr, MAX_NUMBER_LENGTH);
							return EXIT_FAILURE;
						}
						break; // escape while loop
					}
					else if (thisToken == -2 || (thisToken == numbersym && (lastToken == procsym || lastToken == varsym || lastToken == commasym))){
						// identifiers cant be numbers
						if (enableOutput) {
							fprintf(stdout, "Error, identifiers can't be numbers like %s\n", tokenStr);
							return EXIT_FAILURE;
						}
						break; // escape while loop
					}

					// print lexe line by line
					//fprintf(stdout, "%s\t%s \n", tokenStr, getTokenString(thisToken));
					//fprintf(stdout, "%s\t%d \n", tokenStr, thisToken);

					// add to lexeme list
					lexemes[lexemeIndex++] = thisToken;

					// if it is a number or symbol, add the index in the symbol_table to the list
					if (thisToken == numbersym) {
						foundSymbol = findSymbolIndex(tokenStr, symbolIndex);
						lexemes[lexemeIndex++] = symbol_table[foundSymbol].Address;
					}
					else if (thisToken == identsym) {
						foundSymbol = findSymbolIndex(tokenStr, symbolIndex);
						if (foundSymbol != -1)
							lexemes[lexemeIndex++] = symbol_table[foundSymbol].Address;
						else
							lexemes[lexemeIndex++] = -1;
					}
					else if (thisToken == semicolonsym && (lastToken == semicolonsym || (lastToken == endsym && beforeLastToken == semicolonsym)))
					{
						// we have escaped a block and dropped a level
						lexLevel--;
					}

					// start new token, if it isn't white space
					tokenIndex = 0;
					if (!isWhitespace(nextChar)) {
						addCharToToken(&tokenIndex, tokenStr, nextChar);
					}

					// set last two tokens
					beforeLastToken = lastToken;
					lastToken = thisToken;
				}
			}

			tokenEnd = 0;

			// save character to compare later whether it was a symbol or not;
			if (tokenIndex == 1)
				lastChar = tokenStr[tokenIndex - 1];
		}
		nextChar = fgetc(inFile);
	}

	// do all output
	Token lex = 0;
	int i = 0;
	if (enableOutput) {
		if (lexLevel != 0) {
			fprintf(stdout, "Error, lexLevel is %d, missing a semicolon to end a block.", lexLevel);
			return EXIT_FAILURE;
		}

		// 1. "print the tokens' (internal representation) file: "
		for (i = 0; i < lexemeIndex; i++)
		{
			lex = lexemes[i];

			if (lex == identsym)
				fprintf(stdout, "%d %d ", lex, lexemes[++i]);
			else if (lex == numbersym)
				fprintf(stdout, "%d %d ", lex, lexemes[++i]);
			else
				fprintf(stdout, "%d ", lex);
		}
		fprintf(stdout, "\n\n");
	}

	// 2. print lexeme list "symbolic representation"
	i = 0;
	lex = 0;
	FILE* outfile = fopen("lexemelist.txt", "w");
	for (i = 0; i < lexemeIndex; i++)
	{
		lex = lexemes[i];

		if (lex == identsym) {
			i++;
			if (enableOutput)
				fprintf(stdout, "%s %s ", getTokenString(lex), symbol_table[lexemes[i]].Name);
			fprintf(outfile, "%s %s ", getTokenString(lex), symbol_table[lexemes[i]].Name);
		}
		else if (lex == numbersym) {
			i++;
			if (enableOutput)
				fprintf(stdout, "%s %d ", getTokenString(lex), symbol_table[lexemes[i]].Value);
			fprintf(outfile, "%s %d ", getTokenString(lex), symbol_table[lexemes[i]].Value);
		}
		else {
			i++;
			if (enableOutput)
				fprintf(stdout, "%s ", getTokenString(lex));
			fprintf(outfile, "%s ", getTokenString(lex));
		}
	}
	if (enableOutput)
		fprintf(stdout, "\n\n");
	fprintf(outfile, "\n\n");

	fclose(outfile);

	// 3. "A print out of the symbol table"
	outfile = fopen("symboltable.txt", "w");
	Symbol sym;
	for (i = 0; i < symbolIndex; i++) {
		sym = symbol_table[i];
		if (sym.Type != Number) {
			if (enableOutput)
				fprintf(stdout, "%s ", sym.Name);
			fprintf(outfile, "%s ", sym.Name);
		}
		else {
			if (enableOutput)
				fprintf(stdout, "%d ", sym.Value);
			fprintf(outfile, "%d ", sym.Value);
		}
	}
	//fprintf(stdout, "%s", symbols);
	if (enableOutput)
		fprintf(stdout, "\n\n");
	fprintf(outfile, "\n\n");
	fclose(outfile);

	fclose(inFile);
	
	for (i = 0; i < MAX_NUMBER_LEXEMES + MAX_SYMBOL_TABLE_SIZE; i++)
		lexemeList[i] = lexemes[i];
	for (i = 0; i < MAX_SYMBOL_TABLE_SIZE; i++)
		symbolTable[i] = symbol_table[i];

	return EXIT_SUCCESS;
}
