Ian Wyatt

Note:
	This program reads PL/0 of the following EDNF rules:

	program :: = block "." .
	block :: = const - declaration  var - declaration proc - declaration statement.
	constdeclaration :: = [const ident "=" number{ "," ident "=" number } ; "].
	var-declaration  :: = ["var" ident{ "," ident } ; "].							// int
	proc-declaration:: = { "procedure" ident ";" block ";" } statement .
	statement   :: = [
	ident ":=" expression
	| "call" ident
	| "begin" statement{ ";" statement } "end"
	| "if" condition "then" statement[else" statement]
	| "while" condition "do" statement
	| read ident
	| write ident
	| e															// empty
	] .
	condition ::= "odd" expression
	| expression  rel-op  expression.

	rel-op ::= "="|<>"|"<"|"<="|">"|">=.
	expression ::= [ "+"|"-"] term { ("+"|"-") term}.
	term ::= factor {("*"|"/") factor}.
	factor ::= ident | number | "(" expression ").
	number ::= digit {digit}.
	ident ::= letter {letter | digit}.
	digit ;;= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9.
	letter ::= "a" | "b" |  | "y" | "z" | "A" | "B" | ... | "Y" | "Z".

	Reserved Words: const, var, procedure, call, begin, end, if, then, else, while, do, read, write.
	Special Symbols: +, -, *, /, (, ), =, , , .,  <, >,  ; , : .
	Identifiers: identsym = letter (letter | digit)*
	Numbers: numbersym = (digit)+
	Invisible Characters: tab, white spaces, newline
	Comments denoted by: /* . . .   /

Usage:
	