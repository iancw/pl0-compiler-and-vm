﻿#ifdef _WIN32 
#define _CRT_SECURE_NO_DEPRECATE 
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pmachine.h"

#define MAX_STACK_HEIGHT 2000
#define MAX_CODE_LENGTH 500
#define MAX_LEXI_LEVELS 3

// Initial “stack” store values are zeroes.
int stack[MAX_STACK_HEIGHT] = { 0 }; // program stack

// all of our avaiable registers init to 0
int reg[20] = { 0 };

// cheat activation records
int ars = 0; // # activation records
int arPointer[100]; // list of activation record beginnings

// PM / 0 Initial / Default Values :
// Initial values for PM / 0 CPU registers :
// program halts at all pm / 0 cpu register is 0
int SP = 0;
int BP = 1;	// init to 1
int PC = 0;
int IR = 0;
int Halt = 0; // special halt flag
static FILE* outfile;

// var for input/output
int outputEnabled;

// convert operation to string
const char *operationString(Operation f)
{
	const char *strings[] = { "NON", "LIT",
		"RTN",
		"LOD",
		"STO",
		"CAL",
		"INC",
		"JMP",
		"JPC",
		"SIO",
		"SIO",
		"SIO",
		"NEG",
		"ADD",
		"SUB",
		"MUL",
		"DIV",
		"ODD",
		"MOD",
		"EQL",
		"NEQ",
		"LSS",
		"LEQ",
		"GTR",
		"GEQ" };

	return strings[f];
}

// This function will be helpful to find a variable in a different Activation Record some L levels down:
// l stands for L in the instruction format
// Find base L levels down
int base(l, base)
{
	int b1 = base;
	while (l > 0)
	{
		b1 = stack[b1 + 1];
		l--;
	}
	return b1;
}

/* FETCH AND EXECUTE CYCLES */
/* .P - Machine Cycles
The PM / 0 instruction cycle is carried out in two steps.
This means that it executes two steps for each instruction.
The first step is the Fetch Cycle, where the actual instruction is fetched from the “code” memory store.
The second step is the Execute Cycle, where the instruction that was fetched is executed using the “stack” memory store.
This does not mean the instruction is stored in the “stack.”
*/

// the "fetch" cycle
/*
Fetch Cycle :
In the Fetch Cycle, an instruction is fetched from the “code” store and placed in the IR register (IR  code[PC]).
Afterwards, the program counter is incremented by 1 to point to the next instruction to be executed(PC PC + 1).
*/
Instruction fetch(Instruction* code) {
	Instruction nextIR = code[PC];
	PC++;	// always increment after fetch
	return nextIR;
}

// the "execute" cycle
/*
Execute Cycle :
In the Execute Cycle, the instruction that was fetched is executed by the VM.The OP component that is stored
in the IR register (ir.Operation) indicates the operation to be executed.For example,
if ir.Operation is the ISA instruction OPR(ir.Operation = 02),
then the M component of the instruction in the IR register (ir.Modifier) is used to identify the operator
and execute the appropriate arithmetic or logical instruction.
*/
int execute(Instruction ir, int lastPC) {
	switch (ir.Operation) {
	case LIT:
		reg[ir.Register] = ir.Modifier;
		break;
	case RTN:
		SP = BP - 1;
		BP = stack[SP + 3];
		PC = stack[SP + 4];
		ars--;
		break;
	case LOD:
		reg[ir.Register] = stack[base(ir.Level, BP) + ir.Modifier];
		break;
	case STO:
		stack[base(ir.Level, BP) + ir.Modifier] = reg[ir.Register];
		break;
	case CAL:
		stack[SP + 1] = 0;
		stack[SP + 2] = base(ir.Level, BP);
		stack[SP + 3] = BP;
		stack[SP + 4] = PC;
		BP = SP + 1;
		PC = ir.Modifier;

		// handle activation record tracking
		ars++;
		arPointer[ars - 1] = SP;

		break;
	case INC:
		SP += ir.Modifier;
		break;
	case JMP:
		PC = ir.Modifier;
		break;
	case JPC:
		PC = reg[ir.Register] == 0 ? ir.Modifier : PC;
		break;
	case SIO1:
		fprintf(stdout, "%d\n", reg[ir.Register]);
		break;
	case SIO2:
		fprintf(stdout, "Enter a number: ");
		// TODO: flush? fprintf(stderr,"");
		scanf("%d", &reg[ir.Register]); // read(R[i]);
		break;
	case SIO3:
		Halt = 1;
		break;
	case NEG:
		reg[ir.Register] = -reg[ir.Level];
		break;
	case ADD:
		reg[ir.Register] = reg[ir.Level] + reg[ir.Modifier];
		break;
	case SUB:
		reg[ir.Register] = reg[ir.Level] - reg[ir.Modifier];
		break;
	case MUL:
		reg[ir.Register] = reg[ir.Level] * reg[ir.Modifier];
		break;
	case DIV:
		reg[ir.Register] = reg[ir.Level] / reg[ir.Modifier];
		break;
	case ODD:
		reg[ir.Register] = (reg[ir.Level] % 2);
		break;
	case MOD:
		reg[ir.Register] = reg[ir.Level] % reg[ir.Modifier];
		break;
	case EQL:
		reg[ir.Register] = reg[ir.Level] == reg[ir.Modifier];
		break;
	case NEQ:
		reg[ir.Register] = reg[ir.Level] != reg[ir.Modifier];
		break;
	case LSS:
		reg[ir.Register] = reg[ir.Level] < reg[ir.Modifier];
		break;
	case LEQ:
		reg[ir.Register] = reg[ir.Level] <= reg[ir.Modifier];
		break;
	case GTR:
		reg[ir.Register] = reg[ir.Level] > reg[ir.Modifier];
		break;
	case GEQ:
		reg[ir.Register] = reg[ir.Level] >= reg[ir.Modifier];
		break;
	default:
		Halt = 1;
		return EXIT_FAILURE;
		break;
	}
	/*
	Print out the execution of the program in the virtual machine, showing the stack and registers pc, bp, and sp:
	NOTE: It is necessary to separate each Activation Record with a bracket “|”.
	Appendix D
	*/
	// print ir and stack
	char outLine[MAX_STACK_HEIGHT * 12];
	char stackVal[64];
	sprintf(outLine, "%d\t%s\t%d\t%d\t%d\t%d\t%d\t%d", lastPC, operationString(ir.Operation), ir.Register, ir.Level, ir.Modifier, PC, BP, SP);
	int x;
	int y;
	for (x = 0; x <= SP; x++) {
		sprintf(stackVal, "\t%d", stack[x]);
		for (y = 0; y < ars; y++) {
			if (x == arPointer[y]) strcat(stackVal, "\t|");
		}
		strcat(outLine, stackVal);
	}
	strcat(outLine, "\t");
	if (outputEnabled)
		fprintf(stdout, "%s\n", outLine);
	fprintf(outfile, "%s\n", outLine);

	return EXIT_SUCCESS;
}

// do we stop the program?
int isHalted() {
	return (SP == 0 && BP == 0 && PC == 0) || Halt == 1;
}

int virtualize(int enableOutput, Symbol* symbolTable, Instruction* code)
{
	outputEnabled = enableOutput;

	outfile = fopen("stacktrace.txt", "w");
	if (outputEnabled) {
		// print output header
		fprintf(stdout, "\n\t\t\t\t\tpc\tbp\tsp\tstack\n");
		fprintf(stdout, "Initial Values\t\t\t\t%d\t%d\t%d\t%d\n", PC, BP, SP, stack[0]);
	}
	fprintf(outfile, "\n\t\t\t\t\tpc\tbp\tsp\tstack\n");
	fprintf(outfile, "Initial Values\t\t\t\t%d\t%d\t%d\t%d\n", PC, BP, SP, stack[0]);
	
	while (!isHalted()) {
		int error = execute(fetch(code), PC);
		if (error) return EXIT_FAILURE;
	}

	fclose(outfile);

	return EXIT_SUCCESS;
}
