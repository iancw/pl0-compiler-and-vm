﻿#ifdef _WIN32 
#define _CRT_SECURE_NO_DEPRECATE 
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pmachine.h"
#include "scanner.h"

#define MAX_STACK_HEIGHT 2000
#define MAX_CODE_LENGTH 500
#define MAX_LEXI_LEVELS 3
#define OUTPUT_MODE 1
#define MAX_SYMBOL_TABLE_SIZE 100
#define MAX_IDENTIFIER_LENGTH 11
#define MAX_NUMBER_LENGTH 5
#define MAX_NUMBER_LEXEMES 128

int factor();
int expression();
int term();
int program();
int statement();
int block();
int condition();
int emit(Operation Operation, int Register, int Level, int Modifier);

Instruction code[MAX_CODE_LENGTH];
Symbol symbolTable[MAX_SYMBOL_TABLE_SIZE];
Symbol* currentSymbol;
int currentSymbolIndex;
int registerIndex = -1;
int lexemeList[MAX_NUMBER_LEXEMES*MAX_NUMBER_LENGTH];
int tokenIndex;
Token tok; // current token
int error;
int codeIndex = 0; // code index
int hasBegun = 0;
static int lexLevel = -1;
struct Instruction code[MAX_CODE_LENGTH]; // list of instructions

static const char *getErrorString(int e) {
	// Error messages for the tiny PL / 0 Parser:
	static const char* strings[] = {
		"NO ERROR",
		"Error: 1: Use = instead of := .",
		"Error: 2: must be followed by a number.",
		"Error: 3: Identifier must be followed by := .",
		"Error: 4: const, var, procedure must be followed by identifier.",
		"Error: 5: Semicolon expected.",
		"Error: 6: Incorrect symbol after procedure declaration.",
		"Error: 7: Statement expected.",
		"Error: 8: Incorrect symbol after statement part in block.",
		"Error: 9: Period expected.",
		"Error: 10: Semicolon between statements missing.",
		"Error: 11: Undeclared identifier.",
		"Error: 12: Assignment to constant or procedure is not allowed.",
		"Error: 13: Assignment operator expected.",
		"Error: 14: Call must be followed by an identifier.",
		"Error: 15: Call of a constant or variable is meaningless.",
		"Error: 16: \"then\" expected.",
		"Error: 17: Comma expected.",
		"Error: 18: \"Do\" expected.",
		"Error: 19: Incorrect symbol following statement.",
		"Error: 20: Relational operator expected.",
		"Error: 21: Expression must not contain a procedure identifier.",
		"Error: 22: Right parenthesis missing.",
		"Error: 23: The preceeding factor cannot begin with this symbol.",
		"Error: 24: An expression cannot begin with this symbol.",
		"Error: 25: This number is too large.",
		"Error: 26: Number expected.",
		"Error: 27: End expected.",
		"Error: 28: Const must be followed by = .",
		"Error: 29: Code limit exceeded .",
		"Error: 30: Semicolon between blocks missing."
	};
	return strings[e];
}

void nextToken() {
	int x;
	tok = (Token)lexemeList[tokenIndex++];
	if (tok == identsym || tok == numbersym) {
		// we need to get the variable from the symbol table
		x = lexemeList[tokenIndex++];

		if (x != -1) {
			currentSymbolIndex = x;
			currentSymbol = &symbolTable[x];
		}
		else
			currentSymbol = NULL;
	}
}

Symbol symbolAt(int index) {
	return symbolTable[index];
}

int program() {
	int error = 0;
	nextToken();
	error = block(); if (error != 0) return error;

	if (tok != periodsym)return 9; // period expected

	emit(SIO3, 0, 0, 3); // done
	return 0;
}

int block() {
	lexLevel++;
	int error = 0;
	int varCount = 4;
	Symbol *constTarget; // const. helper

	/* Step 1: Generate a jump operation.
	-Store the address of this jump.
	-Keep parsing/generating code.
	-Update the jump address.
	-Update the Procedure address in the Symbol Table */
	int jumpAddress = codeIndex;

	emit(JMP, 0, 0, 0);

	if (tok == constsym) {
		do {
			nextToken();

			if (tok != identsym) return 4; // const must be followed by identifier

			constTarget = currentSymbol; // help const
			nextToken();

			if (tok != eqlsym) {
				if (tok == becomessym)return 1; // "Error: 1: Use = instead of : = .
				return 28; // Const must be followed by =.
			}

			nextToken();

			if (tok != numbersym) return 2; // 2.	= must be followed by a number.

			constTarget->Value = currentSymbol->Value; // declare const;
			nextToken();
		} while (tok == commasym);

		if (tok != semicolonsym) {
			// then ERROR;
			return 5; /* "Error: 5: Semicolon or comma missing.", */
		}
		nextToken();
	}

	if (tok == varsym) {
		do {
			nextToken();

			if (tok != identsym) return 4; // must be followed by identifier

			// set up the address in the stack of this variable
			currentSymbol->Address = varCount;
			varCount++;

			nextToken();
		} while (tok == commasym);

		if (tok != semicolonsym)
			return 5; // must be smeicolon or comma

		nextToken();
	}

	while (tok == procsym) {
		nextToken();

		if (tok != identsym) return 11; // procedure declartion must be followed by a procedure identifier
		if (currentSymbol->Type != Procedure) return 6; // incorrect stymbol after procedure declaration

		currentSymbol->Address = jumpAddress + 1;

		nextToken();

		if (tok != semicolonsym)
			return 6; // incorrect symbol after proc declaration

		nextToken();

		error = block(); if (error != 0) return error;

		if (tok != semicolonsym)
			return 30; // semicolon between blocks missing

		nextToken();
	}

	code[jumpAddress].Modifier = codeIndex; // STEP 1c: Update the jump address.
	emit(INC, 0, 0, varCount); // STEP 2: Reserve Space
	hasBegun = 1;

	error = statement(); if (error != 0) return error; // STEP 3: Generate the procedure code (parse the statements).

	emit(RTN, 0, 0, 0); // STEP 4: Generate a return operation.

	lexLevel--;
	return 0;
}

int statement() {
	int error = 0;
	Symbol *expressionTarget;
	if (tok == identsym) {

		if (currentSymbol == NULL)
			return 11;
		if (currentSymbol->Type != Variable)
			return 12;

		expressionTarget = currentSymbol;

		nextToken();

		if (tok != becomessym)
			return 3;

		nextToken();

		error = expression(); if (error != 0) return error;

		if (tok != semicolonsym)
			return 10; /* "Error: 10: Semicolon between statements missing.", */

		emit(STO, registerIndex, lexLevel - expressionTarget->Level, expressionTarget->Address); // STO: stack[ base(L, bp) + M] = R[i];
		registerIndex--;
	}
	else if (tok == callsym) {
		nextToken();

		if (tok != identsym) return 14; // call symbol must be follwed procedure name
		if (currentSymbol == NULL) return 14; // call symbol must be follwed procedure name
		if (currentSymbol->Type != Procedure) return 15; // Type other than Procedure following Call is meaningless.

		nextToken();

		emit(CAL, 0, lexLevel - currentSymbol->Level, currentSymbol->Address);

		if (tok != semicolonsym)
			return 10; /* "Error: 10: Semicolon between statements missing.", */
	}
	else if (tok == beginsym) {
		nextToken();
		error = statement(); if (error != 0) return error;

		while (tok == semicolonsym) {
			nextToken();
			error = statement(); if (error != 0) return error;
		}

		if (tok != endsym)
			return 27; // end expected

		nextToken();
	}
	else if (tok == ifsym) {
		nextToken();
		error = condition(); if (error != 0) return error;

		if (tok != thensym)
			return 16; // then expected

		nextToken();

		// do jump
		int ctemp = codeIndex;
		registerIndex--; // done with condition block
		emit(JPC, registerIndex, 0, 0); // if R[i] == 0 then { pc = M; }
		registerIndex--;

		error = statement(); if (error != 0) return error;

		if (tok != semicolonsym)
			return 10; /* "Error: 10: Semicolon between statements missing.", */

		code[ctemp].Modifier = codeIndex; // modifier for JPC set here
	}
	else if (tok == whilesym) {
		int codeIndex1 = codeIndex;
		nextToken();
		error = condition(); if (error != 0) return error;
		int codeIndex2 = codeIndex;
		emit(JPC, registerIndex, 0, 0); // if R[i] == 0 then { pc = M; }

		if (tok != dosym)
			return 18; // do expected

		nextToken();

		error = statement(); if (error != 0) return error;

		if (tok != semicolonsym)
			return 10; /* "Error: 10: Semicolon between statements missing.", */

		emit(JMP, 0, 0, codeIndex1);
		code[codeIndex2].Modifier = codeIndex;
	}
	else if (tok == readsym) {
		nextToken();

		if (tok != identsym) return 11;
		if (currentSymbol == NULL) return 11;
		if (currentSymbol->Type != Variable) return 12;

		registerIndex++;
		emit(SIO2, registerIndex, 0, 2); // SIO2: scanf("%d", reg[ir.Register]); // read(R[i]);
		emit(STO, registerIndex, lexLevel - currentSymbol->Level, currentSymbol->Address); // STO: stack[ base(L, bp) + M] = R[i]; 		 
		registerIndex--;

		nextToken();
		if (tok != semicolonsym)
			return 10; /* "Error: 10: Semicolon between statements missing.", */
	}
	else if (tok == writesym) {
		nextToken();

		if (tok != identsym) return 11;
		if (currentSymbol == NULL) return 11;
		if (currentSymbol->Type != Variable) return 12;

		registerIndex++;
		emit(LOD, registerIndex, lexLevel - currentSymbol->Level, currentSymbol->Address);
		emit(SIO1, registerIndex, 0, 1); // SIO1: fprintf(stdout, "%d\n", reg[ir.Register]);
		registerIndex--;

		nextToken();
		if (tok != semicolonsym)
			return 10; /* "Error: 10: Semicolon between statements missing.", */
	}
	else if (!hasBegun) {
		return 7; /* "Error: 7: Statement expected.", */
	}
	return 0;
}

int condition() {
	int error = 0;
	if (tok == oddsym) {
		nextToken();
		error = expression(); if (error != 0) return error;
		emit(ODD, registerIndex, registerIndex - 1, 0); // reg[ir.Register] = (reg[ir.Level] % 2);
	}
	else {
		error = expression(); if (error != 0) return error;

		// relational operator
		int relation = tok;
		if (tok != eqlsym && tok != neqsym && tok != lessym && tok != leqsym && tok != gtrsym && tok != geqsym)
			return 20; // relational operator expected

		nextToken();
		error = expression(); if (error != 0) return error;

		// do condition
		if (relation == eqlsym) {
			emit(EQL, registerIndex - 1, registerIndex - 1, registerIndex); // EQL (R[i] = R[j] == R[k])
		}
		else if (relation == neqsym) {
			emit(NEQ, registerIndex - 1, registerIndex - 1, registerIndex); // NEQ (R[i] = R[j] != R[k])
		}
		else if (relation == lessym) {
			emit(LSS, registerIndex - 1, registerIndex - 1, registerIndex); // LSS (R[i] = R[j] < R[k])
		}
		else if (relation == leqsym) {
			emit(LEQ, registerIndex - 1, registerIndex - 1, registerIndex); // LEQ (R[i] = R[j] <= R[k])
		}
		else if (relation == gtrsym) {
			emit(GTR, registerIndex - 1, registerIndex - 1, registerIndex); // GTR (R[i] = R[j] > R[k])
		}
		else if (relation == geqsym) {
			emit(GEQ, registerIndex - 1, registerIndex - 1, registerIndex); // GEQ  (R[i] = R[j] >= R[k])
		}
		else
			return 20; // relational operator expected
	}
	return 0;
}

int expression() {
	int error = 0;
	int negate = 0;
	if (tok == plussym || tok == minussym) {
		nextToken();
		negate = tok == minussym;
	}

	error = term(); if (error != 0) return error;

	if (negate)
		emit(NEG, registerIndex, registerIndex, 0); // NEG	(R[i] = -R[j])

	while (tok == plussym || tok == minussym) {
		int op = tok;
		nextToken();
		error = term(); if (error != 0) return error;
		if (op == plussym)
			emit(ADD, registerIndex - 1, registerIndex - 1, registerIndex); // ADD (R[i] = R[j] + R[k])
		else
			emit(SUB, registerIndex - 1, registerIndex - 1, registerIndex); // SUB (R[i] = R[j] - R[k])
		registerIndex--;
	}
	return 0;
}

int term()
{
	int error = 0;
	error = factor(); if (error != 0) return error;
	nextToken();
	while (tok == multsym || tok == slashsym) {
		int op = tok;
		nextToken();
		error = factor(); if (error != 0) return error;
		nextToken();
		if (op == multsym)
			emit(MUL, registerIndex - 1, registerIndex - 1, registerIndex); // MUL (R[i] = R[j] * R[k])
		else
			emit(DIV, registerIndex - 1, registerIndex - 1, registerIndex); // DIV (R[i] = R[j] / R[k])
		registerIndex--;
	}
	return 0;
}

int factor()
{
	int error = 0;
	if (tok == identsym) {

		// error check it 
		if (currentSymbol == NULL)
			return 11;
		if (currentSymbol->Type == Number)
			return 12;
		if (currentSymbol->Type == Procedure)
			return 21; /* "Error: 21: Expression must not contain a procedure identifier.", */

		registerIndex++;

		if (currentSymbol->Type == Variable)
			emit(LOD, registerIndex, lexLevel - currentSymbol->Level, currentSymbol->Address);
		else if (currentSymbol->Type == Constant)
			emit(LIT, registerIndex, lexLevel - currentSymbol->Level, currentSymbol->Value);
		else
			return 11;
	}
	else if (tok == numbersym) {
		// error check it 
		if (currentSymbol == NULL)
			return 11;
		else if (currentSymbol->Type != Number)
			return 12;
		else if (currentSymbol->Value >= 100000)
			return 25;

		registerIndex++;

		if (currentSymbol->Type == Number)
			emit(LIT, registerIndex, 0, currentSymbol->Value);
		else
			return 26;
	}
	else if (tok == lparentsym) {
		nextToken();
		error = expression(); if (error != 0) return error;
		if (tok != rparentsym)
			return 22; /* "Error: 22: Right parenthesis missing.", */
		nextToken();
	}
	else {
		// ERROR
		return 23; // The preceeding factor cannot begin with this symbol.
	}
	return 0;
}

// emit opcode to code list
int emit(Operation Operation, int Register, int Level, int Modifier)
{
	if (codeIndex > MAX_CODE_LENGTH)
		return 29; // code limit exceeded
	else
	{
		code[codeIndex].Operation = Operation; 	// opcode
		code[codeIndex].Register = Register; 	// register
		code[codeIndex].Level = Level;		// lexicographical level
		code[codeIndex].Modifier = Modifier;		// modifier
		codeIndex++;
	}
	return 0;
}

int main(int argc, char **argv)
{
	char c;
	int printLexemes = 0;
	int printGeneratedAssembly = 0;
	int printPmachineTrace = 0;
	tokenIndex = 0; // MUST INITIALIZE THIS TO 0
	/*FILE* outFile = fopen("output.txt", "w");*/ // not using output files for this one

	if (argc == 1) {
		//Print nothing to the console except for “in” and “out”
		fprintf(stdout, "in\n");
	}
	else {
		int argCount;
		for (argCount = 1; argCount < argc; argCount++){
			if (strcmp(argv[argCount], "-v") == 0) {
				// print virtual machine execution trace (virtual machine output) to the screen
				printPmachineTrace = 1;
			}
			else if (strcmp(argv[argCount], "-a") == 0) {
				// print the generated assembly code (parser/codegen output) to the screen
				printGeneratedAssembly = 1;
			}
			else if (strcmp(argv[argCount], "-l") == 0) {
				// print the list of lexemes/tokens (scanner output) to the screen
				printLexemes = 1;
			}
			else {
				// error
				fprintf(stdout, "ERROR: %s is not a recognized command. Your options are -v, -a, -l, or none.", argv[argCount]);
			}
		}
	}

	// for debugging
#ifdef _WIN32 
	printLexemes = 1;
	printGeneratedAssembly = 1;
	printPmachineTrace = 1;
#endif

	// begin
	int error;
	error = scan("input.txt", printLexemes, lexemeList, symbolTable);
	if (!error) {

		// start parser / syntax check
		error = program();

		if (error) {
			if (printGeneratedAssembly)
				fprintf(stdout, "The following error occurred during parsing: %s\n", getErrorString(error));
		}
		else {
			if (printGeneratedAssembly)
				fprintf(stdout, "No errors, the program is syntactically correct.\n\n");
			// print out code
			int n;
			FILE* outfile = fopen("ICG.txt", "w");
			for (n = 0; n < codeIndex; n++) {
				if (printGeneratedAssembly)
					fprintf(stdout, "%d %s %d %d %d\n", n, operationString(code[n].Operation), code[n].Register, code[n].Level, code[n].Modifier);
				fprintf(outfile, "%d %s %d %d %d\n", n, operationString(code[n].Operation), code[n].Register, code[n].Level, code[n].Modifier);
			}
			if (printGeneratedAssembly)
				fprintf(stdout, "\n");
			fprintf(outfile, "\n");
			fclose(outfile);

			error = virtualize(printPmachineTrace, symbolTable, code);
			if (!error) {
				if (argc == 1) {
					//Print nothing to the console except for “in” and “out”
					fprintf(stdout, "out\n");
				}
			}
		}
	}

	// allow the user to see the input before exiting the program
	fprintf(stdout, "Press any key to exit...");
	while ((c = getchar()) != '\n' && c != EOF); // TODO: does this work?
	getchar();
	return EXIT_SUCCESS;
}