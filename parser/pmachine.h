#ifndef P_MACHINE
#define P_MACHINE
#include "scanner.h"

// operations enumeration
typedef enum {
	LIT = 1,
	RTN,
	LOD,
	STO,
	CAL,
	INC,
	JMP,
	JPC,
	SIO1,
	SIO2,
	SIO3,
	NEG,
	ADD,
	SUB,
	MUL,
	DIV,
	ODD,
	MOD,
	EQL,
	NEQ,
	LSS,
	LEQ,
	GTR,
	GEQ
} Operation;

typedef struct Instruction{
	Operation Operation;		//opcode
	int Register;		// R
	int Level;			// L
	int Modifier;	// M
} Instruction;

// convert operation to string
const char *operationString(Operation f);
int virtualize(int enableOutput, Symbol* symbolTable, Instruction* code);
#endif