#ifndef SCANNER
#define SCANNER

// a numerical value is assigned to each token
typedef enum {
	nulsym = 1, identsym = 2, numbersym = 3, plussym = 4, minussym = 5, multsym = 6, slashsym = 7,
	oddsym = 8, eqlsym = 9, neqsym = 10, lessym = 11, leqsym = 12, gtrsym = 13, geqsym = 14,
	lparentsym = 15, rparentsym = 16, commasym = 17, semicolonsym = 18, periodsym = 19, becomessym = 20,
	beginsym = 21, endsym = 22, ifsym = 23, thensym = 24, whilesym = 25, dosym = 26, callsym = 27,
	constsym = 28, varsym = 29, procsym = 30, writesym = 31, readsym = 32, elsesym = 33
} Token;

typedef enum {
	Constant = 1, Variable = 2, Procedure = 3, Number = 4
} SymbolType;

typedef struct
{
	SymbolType Type; 	// const = 1, var = 2, proc = 3, Number = 4
	char Name[10];		// name up to 11 chars
	int Value; 			// numerical value if Constant or Number
	int Level; 			// L level
	int Address; 		// M address
} Symbol;

#define MAX_SYMBOL_TABLE_SIZE 100
#define MAX_IDENTIFIER_LENGTH 11
#define MAX_NUMBER_LENGTH 5
#define MAX_NUMBER_LEXEMES 128

int scan(char* inFileName, int enableOutput, int lexemeList[MAX_NUMBER_LEXEMES*MAX_NUMBER_LENGTH], Symbol symbolTable[MAX_SYMBOL_TABLE_SIZE]);
int findSymbolIndex(char* symbolName, int symbolIndex);
int findNumberIndex(int symbolValue);
const char *getTokenString(Token t);

#endif